const textelement = ['Saya Adalah Mahasiswa','Universitas Ahmad Dahlan','Program Studi Informatika','2021'];
let count = 0;
let textindex = 0;
let currenttxt = '';
let words = '';

(function ngetik(){

	if(count == textelement.length){
		count = 0;
	}

	currenttxt = textelement[count];

	words = currenttxt.slice(0, ++textindex);
	document.querySelector('.efek-ngetik').textContent = words;

	if(words.length == currenttxt.length){
		count++;
		textindex = 0;
	}

	setTimeout(ngetik,500);

})();